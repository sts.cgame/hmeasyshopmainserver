import { getDatabase,set,ref,get } from "firebase/database";
import { initializeApp } from "firebase/app";
import express from "express";
import bodyParser from "body-parser";

var appExpress = express();
appExpress.use(bodyParser.json());
appExpress.use(bodyParser.urlencoded({ extended: true }));
var server = appExpress.listen(3001,console.log('Server is runing on port 3001'))

const firebaseConfig = {
    databaseURL:"https://handmade-3e4a3-default-rtdb.asia-southeast1.firebasedatabase.app/"
}
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);

appExpress.get('/', function (req, res) {
    res.status(200).json({status:200,message:'Welcome my server'});
})

appExpress.post('/api/create',(req,res) => {
    var fullname = req.body.fullname
    
    try {
        console.log('fullname : ',fullname)
        set(ref(db,'users/'+fullname),{
            name:fullname,
            balance:100,
            mil:new Date().getTime(),
            date:new Date() + ''
        })
        return res.status(200).json({status:200,message:'success'});
    } catch (error) {
        return res.status(500).json({status:500,message:error.message});
    }
})

appExpress.get('/api/get',async (req,res,next) => {
    try {
     
        await get(ref(db,'users'))
        .then((snapshot) => {
            console.log('snapshot : ',snapshot.val())
            if(snapshot.exists()) {
               return res.status(200).json({status:200,message:'success',result: snapshot.val()})
            }else {
                return res.status(404).json({status:404,message:'Not found'})
            }
        })
        // .catch ((error) => {
        //     console.log('error : ',error)
        // })
        // return res.status(200).json({status:200,message:'success'});
    } catch (error) {
        return res.status(500).json({status:500,message:error.message});
    }
})